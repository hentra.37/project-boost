using UnityEngine;
using UnityEngine.SceneManagement;

public class CollisionHandler : MonoBehaviour
{
    void OnCollisionEnter(Collision other) 
    {
        switch (other.gameObject.tag) 
        {
            case "Friendly":
                Debug.Log("This object is friendly!");
                break;
            case "Finish":
                Debug.Log("Congration! You done it");
                break;
            case "Fuel":
                Debug.Log("You picked up fuel!");
                break;
            default:
                ReloadLevel();
                break;
        }
    }
    void ReloadLevel()
    {
        SceneManager.LoadScene(0);
    }
}
